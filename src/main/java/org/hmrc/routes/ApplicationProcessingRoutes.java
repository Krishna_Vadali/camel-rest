package org.hmrc.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.hmrc.processor.ErrorProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class ApplicationProcessingRoutes extends RouteBuilder {

    @Autowired
    private ErrorProcessor errorProcessor;

    @Override
    public void configure() throws Exception {

        restConfiguration().component("servlet").port(9090).host("localhost")
                .bindingMode(RestBindingMode.json)
                .clientRequestValidation(true).dataFormatProperty("prettyPrint", "true");

        rest("/api").post("/post-application")
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .produces(MediaType.APPLICATION_JSON_VALUE)
                 .bindingMode(RestBindingMode.off)
                .to("direct:applicationProcessing");

        from("direct:applicationProcessing")
                .onException(Exception.class).process(errorProcessor).handled(true).end()
                .to("log:?level=INFO&showBody=true")
                .log("${body}")
                .convertBodyTo(String.class)
                .to("json-validator:applicationschema.json");



    }
}
